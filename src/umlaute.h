#pragma once
#ifndef UMLAUTEH_INCLUDED
#define UMLAUTEH_INCLUDED

    //Ist Windows
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    #define AE (unsigned char)142
    #define ae (unsigned char)132
    #define OE (unsigned char)153
    #define oe (unsigned char)148
    #define UE (unsigned char)154
    #define ue (unsigned char)129
    #define ss (unsigned char)225
    #else
    //Ist nicht Windows (also Unix)
    //Leider keine Ahnung wie man das unter Linux konsistent hinbekommt :( Fallback zu darstellung durch Nicht-Umlaute
    #define AE 'A'
    #define ae 'a'
    #define OE 'O'
    #define oe 'o'
    #define UE 'U'
    #define ue 'u'
    #define ss 's'
    #endif
#endif