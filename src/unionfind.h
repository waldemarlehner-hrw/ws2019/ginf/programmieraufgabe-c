#pragma once
#ifndef UNIONFINDH_INCLUDED
#define UNIONFINDH_INCLUDED

/*
 * Headerdatei für Union-Find Operationen
 * 
 */

//Ein Struct, welches ein Element der UnionFind Datenstruktur beschreibt 
typedef struct UF_Reference {
    /* 
     * Verbindung zum Elternteil. Repräsentant
     * zeigt auf sich selbst. Man kann die Adressen
     * den Pointer vergleichen, um zu schauen ob zwei
     * Elemente denselben Repräsentanten besitzen
     */
    struct UF_Reference* parent;
    /*
     * Die ID des Knotens. Nur die Repräsanten-Knoten-ID
     * ist relevant. UNION-FIND lässt sich auch ohne ID
     * implementieren, ist jedoch für das Anzeigen der
     * Repräsentanten nötig
     */
    int vertexId;
   
} UF_Reference;

// Findet den Repräsentant eines Elementes der Union-Find Datenstruktur
UF_Reference* UF_Find(UF_Reference* element){
    struct UF_Reference* ref = element;
    
    while(ref != (ref->parent)){

        ref = (ref->parent);
    }
    return ref;
}

//Eine Optimierung der Union-Find Datenstruktur. Dabei werden alle Knoten 
//entlang des Pfades zum Repräsentanten direkt auf den Repräsentanten gezeigt
void UF_Flatten(UF_Reference* element, UF_Reference* representative){
    
    while(representative != element->parent && element != element->parent){
        UF_Reference* next = element->parent;
        element[0].parent = representative;
        element = next;
    }
} 
//Führe zwei Union-Find Sets zusammen, hänge dabei ein Element an das andere, optimiere dann die Struktur mit UF_Flatten
UF_Reference* UF_Union(UF_Reference* element1,UF_Reference* element2){
    /*
     * Beide Elemente zeigen auf denselben Repräsentanten, es wird 
     * keine UNION-Operation benötigt
     */
    UF_Reference* representative_el1 = UF_Find(element1);
    UF_Reference* representative_el2 = UF_Find(element2);
    if(representative_el1 == representative_el2){
  
        return UF_Find(element1);
    }
    /*
     * Setze die Parent-Referenz eines Elementes auf die des anderen.
     * Führe dann UF_Flatten aus, um die Datenstruktur zu
     * optimieren
     */
    representative_el1[0].parent = representative_el2;
    UF_Flatten(element1,representative_el2);
    UF_Flatten(element2,representative_el2);
}
#endif


