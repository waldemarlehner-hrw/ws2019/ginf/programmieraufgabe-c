#include "userInput.h"
#include "output.h"
#include "color.h"
#include "kruskalPrepare.h"
#include "unionfind.h"
//überspringe matrix aufsetzung um beim testen nicht immer den Spaß eingeben zu müssen
//#define SKIPMATRIXINIT



int main(void){
	//Wenn Windows, hol dir die Konsolenparameter, um farben manipulieren zu können
	#ifdef IS_WINDOWS_ENVIRONMENT
		windowsColorHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		GetConsoleScreenBufferInfo(windowsColorHandle,&consoleInfo);
		defaultConsoleAttribures = consoleInfo.wAttributes;
	#endif


	consoleChangeColor(Info);
	printInitOutput();
	consoleChangeColor(Default);
	#pragma region Aufsetzen der Matrix
	
	printf("Wie viele Knoten soll die Abstandsmatrix repr%csentieren?\n",ae);
	int vertexCount = userInput_getInt(true,false,false);
	
	
	clear();

	while(vertexCount < 2){
		consoleChangeColor(Error);
		printf("[FEHLER] Man ben%ctigt mindestens 2 Knoten. Bitten geben wie einen Wert gr%c%cer als 1 an.\n",oe,oe,ss);
		consoleChangeColor(Default);
		vertexCount = userInput_getInt(true,false,false);
	}
	int distanceMatrix[vertexCount*vertexCount];


	for(int i = 0;i<vertexCount*vertexCount;i++){
		distanceMatrix[i] = (i%(vertexCount+1) == 0)?0:INT_MAX; //Unendlich als INT_MAX definiert. Fülle mit Unendlich auf. 
	}
	//Überschreibe nun noch die Hauptdiagonale mit Nullen

	clear();
	//Erstelle einen CharakterBuffer. Dabei hat jeder Knoten eine Namen mit einer beliebigen Länge. Nur der Pointer wird gespeichert. Die Positionen der Speicher werden mit malloc abgespeichert
	char* vertexNames[vertexCount];
	//Frage den Nutzer ob er Namen für die Knoten eingeben will.
	printf("M%cchten Sie den Knoten Namen vergeben?\n",oe);
	if(userInput_getBoolean()){
		//Der Nutzer möchte die Namen selbst vergeben.
		// i = Iterator für die Aktuelle Eingabe
		// j = Iterator um frühere eingaben Darzustellen
		for(int i = 0;i<vertexCount;i++){
			clear();
			for(int j = 0;j<i;j++){
				printf("[%i] >\t %s",j+1,vertexNames[j]);
				printf("\n");
			}
		
			printf("\n\n\n");
			printf("Geben Sie den Namen f%cr den Knoten Nr %i ein. Eine leere Eingabe erstellt einen automatischen Namen.\n",ue,i+1);
			char* name;
			name = userInput_getString(2048,true);
			if(name == NULL){
				//Die Eingabe war leer. Erstelle eigenen Namen
				char buff[21];
				sprintf(buff,"Knoten %i",i+1);
				int len = getStringLen(buff);
				name = (char*) malloc(sizeof(char)*len);
				for(int i = 0;i<len;i++){
					name[i] = buff[i];
				}
				
				
			}
			//Jetzt ist entweder der vom Nutzer eingegebene oder autogenerierte Name im name-Char Array
			//Füge nun den Namen zum vertexNames Array hinzu.
			vertexNames[i] = name;
		

		}
	}else{
		//Vergebe die Namen 'Knoten X' (X = index), wenn kein eigener Name angegeben wird
		
		for(int i = 0;i<vertexCount;i++){
			char name[21];
			sprintf(name,"Knoten %i",i+1);
			int nameLen = getStringLen(name);
			vertexNames[i] = (char*) malloc(sizeof(char)*nameLen);
			for(int j = 0;j<nameLen;j++){
				vertexNames[i][j] = name[j]; // Übertrage den Namen aus dem Char Buffer rüber in den Array, welcher alle Namen speichert
			}
			
		}
	}
	clear();
	//Zeige die Matrix
	DistanceMatrixConf:
	consoleChangeColor(Info);
	printf("Wie soll der Graph konfiguriert werden?\n[Y] Abstandsmatrix (gut f%cr grosse Graphen)\n[N] Durchgehen aller Kanten (gut f%cr kleine Graphen)\n",ue,ue);
	if(!userInput_getBoolean()){
		//Gehe nur den Teil unter der Hauptdiagonal der Abstandsmatrix durch, damit man nicht dieselbe Kante 2x angeben muss
		for(int i = 0;i<vertexCount;i++){
			for(int j = i+1;j<vertexCount;j++){
				
				
				printf("[%i] %s <-> [%i] %s hat ein Kantengewicht von ?\nGeben Sie eine ganze Zahl oder eine leere Eingabe an. Bei einer leeren Eingabe gibt es keine Verbindung.\n\n",i+1,vertexNames[i],j+1,vertexNames[j]);
				int edgeWeight = userInput_getInt(false,true,true);
				distanceMatrix[i*vertexCount+j] = edgeWeight;
				distanceMatrix[j*vertexCount+i] = edgeWeight;
				clear();
			}
		}
		printDistanceMatrixLegend();
		printDistanceMatrix(vertexCount,vertexNames,distanceMatrix);
		printf("Soll die Abstandsmatrix noch modifiziert werden?\nBei Nein geht es weiter\n");
		if(!userInput_getBoolean()){
			goto AfterDistanceMatrixSetup;
		}
		clear();

	}
		
	
	printDistanceMatrixLegend();
	printDistanceMatrix(vertexCount,vertexNames,distanceMatrix);
	//Die Matrix wurde angezeigt. Jetzt darf sie vom Nutzer manipuliert werden.
	//Drücke einen Hübschen Header aus.
	consoleChangeColor(Info);
	printf(
		"\n\n\n"
		"================================\n"
		"Konfiguration der Abstandsmatrix\n"
		"================================"
		"\n\n\n");
	consoleChangeColor(Default);
	int userWantsToContinueConfiguration = true;
	while(userWantsToContinueConfiguration){
		
		
		printf(" ? <-> ? hat ein Kantengewicht von ?\n");
		consoleChangeColor(Gray);
		printf("Geben sie den ersten Knoten der Verbindung an. Erlaubte Eingabe: [%i ... %i]\n",1,vertexCount);
		consoleChangeColor(Default);
		int knoten1ID = userInput_getInt(true,false,false)-1;
		while(knoten1ID  > vertexCount-1){
			consoleChangeColor(Error);
			printf("[FEHLER] Die Eingabe darf nicht gr%c%cer als %i sein.\n",oe,ss,vertexCount);
			knoten1ID = userInput_getInt(true,false,false)-1;
		}
		consoleChangeColor(Default);
	
		printf(" [%i] %s<-> ? hat ein Kantengewicht von ?\n",knoten1ID+1,vertexNames[knoten1ID]);
		consoleChangeColor(Default);
		int knoten2ID = userInput_getInt(true,false,false)-1 ;
		while(knoten2ID > vertexCount-1 || knoten2ID == knoten1ID){
			consoleChangeColor(Error);
			if(knoten1ID == knoten2ID){
				printf("[FEHLER] Die beiden Knoten d%crfen nicht identisch sein. Bitte geben Sie f%cr den zweiten Knoten einen anderen Knoten als [%i] an\n",ue,ue,knoten1ID+1);
			}
			else{
			printf("[FEHLER] Die Eingabe darf nicht gr%c%cer als %i sein.\n",oe,ss,vertexCount);
			}
			knoten2ID = userInput_getInt(true,false,false)-1 ;
		}
		consoleChangeColor(Default);
	
		printf(" [%i] %s <-> [%i] %s hat ein Kantengewicht von ?\n",knoten1ID+1,vertexNames[knoten1ID],knoten2ID+1,vertexNames[knoten2ID]);
		consoleChangeColor(Default);
		printf("Geben sie ein Kantengewicht als ganze Zahl an. Leere Eingabe trennt die Verbindung zweier Knoten!\n");
		int kantenGewicht = userInput_getInt(false,true,true);
		
		consoleChangeColor(Info);
		printf("\n\n\n\n##################\n");
		printf("Stimmt diese Eingabe?\n");
		
		if(kantenGewicht != INT_MAX)
		printf(" %i <-> %i hat ein Kantengewicht von %i\n",knoten1ID+1,knoten2ID+1,kantenGewicht);
		else
		printf(" %i <-> %i wurde entfernt.\n",knoten1ID+1,knoten2ID+1);
		printf("###\n");
		consoleChangeColor(Default);
		printf("y: Ja, hinzuf%cegen.\n",ue);
		printf("Y: Ja, hinzuf%cgen und nach keinen weiteren Kanten fragen.\n\n",ue);
		printf("n: Nein, nicht hinzuf%cgen.\n",ue);
		printf("N: Nein, und nicht nach weiteren Kanten fragen.\n");
		printf("###\n");
			
		char userInputOperation = userInput_getChar();
		while(
			userInputOperation != 'Y' &&
			userInputOperation != 'y' &&
			userInputOperation != 'N' &&
			userInputOperation != 'n' 
			){
				consoleChangeColor(Error);
				printf("[FEHLER] Gegebene Eingabe muss Y,y,N oder n sein.\n");
				userInputOperation = userInput_getChar();
				consoleChangeColor(Default);
		}
		if(userInputOperation == 'y' || userInputOperation == 'Y'){
			distanceMatrix[knoten1ID*vertexCount+knoten2ID] = kantenGewicht;
			
			distanceMatrix[knoten2ID*vertexCount+knoten1ID] = kantenGewicht;
			
			if(userInputOperation == 'Y'){
				userWantsToContinueConfiguration = false;
			}
		}
		if(userInputOperation == 'N'){
			userWantsToContinueConfiguration = false;
		}
		clear();
		printDistanceMatrixLegend();
		printDistanceMatrix(vertexCount,vertexNames,distanceMatrix);



	}
	#pragma endregion
	#pragma region Vorbereitung Kruskal

	AfterDistanceMatrixSetup:
	consoleChangeColor(Default);
	printf("%cberpr%cfe ob der Graph zusammenh%cngend ist\n",UE,ue,ae);
	if(!istZusammenhaengend(distanceMatrix,vertexCount)){
		consoleChangeColor(Error);
		printf("[FEHLER] Der angegebene Graph ist nicht zusammenh%cngend! Bitte %cndern sie den Graphen um.\n\n",ae,ae);
		consoleChangeColor(Default);
		goto DistanceMatrixConf;
	}

	printf("Soll ein Maximal- oder Minimalspannbaum erzeugt werden?\n");
	consoleChangeColor(Info);
	printf("[Y]: Maximalspannbaum\n[N]: Minimalspannbaum\n\n");
	consoleChangeColor(Default);
	int useMaxSpanningTree = userInput_getBoolean();
	#pragma endregion
	#pragma region Kruskal-Implementierung
	//Wandle alle Vorhandenen Kanten in structs um
	int edgeCount = getEdgeCount(distanceMatrix,vertexCount);
	
	GRAPH_EDGE* Edges = generateEdgeArray(distanceMatrix,vertexCount,edgeCount);
	sortEdges(Edges,edgeCount,useMaxSpanningTree);
	clear();
	
	printf("Sortierte Kanten ");
	if(useMaxSpanningTree){
		printf("Gross nach Klein");
	}else{
		printf("Klein nach Gross");
	}
	printf("\n\n");
	printEdgeTable(Edges,edgeCount,vertexNames);
	
	consoleChangeColor(Default);
	printf("\n Eine beliebige Taste eingeben...\n");
	getchar();
	
	UF_Reference* UnionFindMap = malloc(sizeof(UF_Reference)*vertexCount);
	for(int i=0;i<vertexCount;i++){
			
			UnionFindMap[i] = (UF_Reference){NULL,i};
			UnionFindMap[i].parent = (UF_Reference*)&UnionFindMap[i];
			
	}

	//Beginne Kruskal. Es wird ein neuer Scope {} erstellt, da die Variablen nach dem Durchlauf nicht mehr benötigt werden
	{
		//Gibt an welche Kanten teil des Min/Max Spanning Tree sind
		int EdgesMap[edgeCount];
		//Die Union Find Datenstruktur
		//Initialisiere UF Datenstruktur
		

		//Speicher den Vorgang in einem Array, nutze den Array dann um die Kruskal-Tabelle auszugeben
		

		struct Kruskal_Table_Entry kruskalTableEntries[edgeCount];
		
		
	
		for(int i = 0;i<edgeCount;i++){
			EdgesMap[i] = 0; //Initialisieren als "nicht im MST"
			int vert1 = Edges[i].VERTEX1;
			int vert2 = Edges[i].VERTEX2;
			UF_Reference* rep1 = UF_Find(&UnionFindMap[vert1]);
			UF_Reference* rep2 = UF_Find(&UnionFindMap[vert2]);
			if(rep1 == rep2){
				//Beide Kanten haben denselben Repräsentanten
				//Das hinzufügen würde einen Zyklus erzeugen -> Überspringe
				Kruskal_Table_Entry te = {vert1,vert2,Edges[i].WEIGHT,rep1->vertexId,rep2->vertexId,0};
				kruskalTableEntries[i] = te;
				continue;
			}
			Kruskal_Table_Entry te = {vert1,vert2,Edges[i].WEIGHT,rep1->vertexId,rep2->vertexId,1};
			kruskalTableEntries[i] = te;
			UF_Union(&(UnionFindMap[vert1]),&(UnionFindMap[vert2]));
			EdgesMap[i] = 1;
			

		}
		printKruskal(vertexNames,vertexCount,edgeCount,UnionFindMap,kruskalTableEntries,Edges);

		/*
		 * Gehe alle Kanten durch. Wenn EdgesMap[i] != 0 ist, so ist die Kante
		 * Teil des MST. Gebe die Kante in der Konsole aus
		 */
		int weightSumMST = 0;
		printf(
			"-------------------------------------------------------\n"
			"               Kanten, die den MST bilden              \n"
			"-------------------------------------------------------\n"
			);
		for(int i = 0;i<edgeCount;i++){
			if(!EdgesMap[i])
				continue;
			weightSumMST += Edges[i].WEIGHT;
			printf("[%i] %s <- %i -> [%i] %s\n",
			Edges[i].VERTEX1,vertexNames[Edges[i].VERTEX1],
			Edges[i].WEIGHT,
			Edges[i].VERTEX2,vertexNames[Edges[i].VERTEX2]); 
		
		}
		printf(
			"-------------------------------------------------------\n"
			"Gesamtgewicht:"
			);
		consoleChangeColor(Info);
		printf("%i\n\n",weightSumMST);
		consoleChangeColor(Default);
		

	}



	


	


	#pragma endregion
	printf("\n\n");
	//Nicht mehr benötigter mit malloc definierter Speicher wird als unbenutzt definiert
	free(Edges);
	free(UnionFindMap);

	#ifndef SKIPMATRIXINIT
	//Frage den Nutzer ob er änderungen an der Abstandsmatrix machen will und es nochmal durchführen möchte.
	printf("==================================================================\n"); 
	printf("Die Ausf%chrung ist fertig. M%cchten sie zur%cck zur Abstandsmatrix\nkonfiguration und %cnderungen vornehmen? (Nein beendet das Programm)\n",ue,oe,ue,AE); \
	printf("==================================================================\n"); 
	printf("\n\n");
	if(userInput_getBoolean()){
		//Gehe zur Abstandsmatrixkonfiguration
		goto DistanceMatrixConf;
	}else{
		return EXIT_SUCCESS;
	}
	#else
	scanf("%c");
	return EXIT_SUCCESS;
	#endif
	
	

}