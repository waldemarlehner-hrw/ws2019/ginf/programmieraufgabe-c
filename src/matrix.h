
#pragma once
#ifndef MATRIXH_INCLUDED
#define MATRIXH_INCLUDED
//Ein Header für Funktionen der 2D-Arrays

//Da das übergeben von 2D Arrays (oder pointern auf pointer) doch eher mehr Probleme mit \
sich bringt verwenden wir einen linearisierten Array. D.h. die Zeilen wurden hintereinander gelegt
int getLinearizedArrayIndex(int zeilenIndex,int spaltenIntex,int spaltenAnzahl){
	return zeilenIndex*spaltenAnzahl+spaltenIntex;
}
#endif