#ifndef KRUSKALPREPAREH_INCLUDED
#define KRUSKALPREPAREH_INCLUDED
#endif
#pragma once
//#define DEBUG
//Funktionen für das Vorbereiten des Kruskal Algorithmus, d.h. sortieren, konvertiere usw.

#include "matrix.h"
#include <limits.h>


// Definiere ein Strukt, um Kanten zu speichern. Beinhaltet die IDs der beiden Knoten und das Gewicht
typedef struct GRAPH_EDGE{
    int VERTEX1;    //ID vom 1. Knoten  !!! Dabei ist ID von K1 IMMER < ID von K2
    int VERTEX2;    //ID vom 2. Knoten
    int WEIGHT;     //Kantengewicht
} GRAPH_EDGE;


// Ein Struct, welches einen Eintrag in die Kruskal-Tabellen-Ausgabe repräsentiert
typedef struct Kruskal_Table_Entry {
			int v1;
			int v2;
			int weight;
			int rep1;
			int rep2;
			int hasbeenAdded;
    } Kruskal_Table_Entry;

//Gibt die Anzahl der Kanten aus, welche in der Abstandsmatrix gespeichert sind
int getEdgeCount(int* distanceMatrix, int vertexCount){
    //In der Matrix ist jede Kante zwei mal Eingetragen. Wenn man jedoch nur alle Werte unterhalb der Hauptdiagonale betrachtet, wird jede Kante nur ein mal beobachtet

    int count = 0;
    for(int i = 0;i<vertexCount;i++){
        for(int j = i+1;j<vertexCount;j++){
            if(distanceMatrix[getLinearizedArrayIndex(i,j,vertexCount)] != INT_MAX){
                count++;
            }
        }
    }
    return count;
}
/**
 * Verwendet Selectionsort zum Sortieren des Arrays. Bogosort wäre uns Lieber, aber naja.. 
 * Punktetechnisch wäre das dann eher "suboptimal"
 * 
 * @param {struct GRAPH_EDGE*} edgeArray Ein Array an Kanten-Structs
 * @param {int} useMaximum wenn !0 wird groß -> klein sortiert, ansonsten klein -> groß 
 * @param {int} edgeCount Anzahl an Kanten im Array
 * @returns {void} Es wird nichts zurückgegeben, da die Speicheradressen sich nicht verändern.
 */
void sortEdges(GRAPH_EDGE* edgeArray,int edgeCount,int useMaximum){
    
    for(int i=0;i<edgeCount-1;i++){

        for(int j=i+1;j < edgeCount;j++){
            if(useMaximum){
                if(edgeArray[i].WEIGHT < edgeArray[j].WEIGHT){
                    GRAPH_EDGE temp = edgeArray[i];
                    edgeArray[i] = edgeArray[j];
                    edgeArray[j] = temp;
                }
                    
                
            }else{
                if(edgeArray[i].WEIGHT > edgeArray[j].WEIGHT){
                    GRAPH_EDGE temp = edgeArray[i];
                    edgeArray[i] = edgeArray[j];
                    edgeArray[j] = temp;

                }
            }
        }

    }
    #ifdef DEBUG
    for(int i=0;i<edgeCount;i++){
        consoleChangeColor(Error);
        printf("<%i,%i,%i>",edgeArray[i].VERTEX1+1,edgeArray[i].VERTEX2+1,edgeArray[i].WEIGHT);
        consoleChangeColor(Default);
    }
    #endif
    return;
}

//Erstelle einen Array an Kanten. Der Speicher dafür wird mit malloc reserviert
GRAPH_EDGE* generateEdgeArray(int* distanceMatrix,int vertexCount,int edgeCount){

    GRAPH_EDGE* ptr = malloc(sizeof(GRAPH_EDGE)*edgeCount);
    int currentIndex = 0;
    for(int i = 0;i<vertexCount;i++){
        for(int j = i+1;j<vertexCount;j++){
            int weight = distanceMatrix[getLinearizedArrayIndex(i,j,vertexCount)];
            if(weight != INT_MAX){
                int v1 = i;
                int v2 = j;
                //Swap
                if(v1 > v2){
                    int z = v1;
                    v1 = v2;
                    v2 = z;
                }
                GRAPH_EDGE edge = {v1,v2,weight};
                #ifdef DEBUG
                consoleChangeColor(Warn);
                printf("\nErstelle Kante: <%i,%i,%i>\n",v1+1,v2+1,weight);
                consoleChangeColor(Default);
                #endif
                ptr[currentIndex++] = edge;

            }
        }
    }
    return ptr;
}

//Eine Funktion, welche Aussagt, ob der durch die Abstandsmatrix repräsentierte Graph zusammenhängend ist, also man von jedem Knoten zu allen anderen gelangen kann
int istZusammenhaengend(int* distanceMatrix,int vertexCount){

    int addedVertices[vertexCount]; //Quasi "Bitmap", speichert ab ob ein Knoten schon aufgenommen wurde bei der Überprüfung.
    //Man muss den Speicher initialisieren, da es sonst undefiniertes Verhalten gibt.
    for(int i = 1;i<vertexCount;i++){
        addedVertices[i] = 0;
    }
    //Definiere den ersten Knoten als "im Graph"
    addedVertices[0] = 1;
    for(int b = 0; b < vertexCount-1; b++){ //Man benötigt maximal |V|-1 Schritte, um von einem Knoten alle zu erreichen
       
        for(int j = 0;j<vertexCount;j++){
      
            if(addedVertices[j]){ //Wenn ist Teil des Graphen
                //Füge alle am aktuellen Knoten hängenden Knoten zu dem aufgenommenen Knoten hinzu
                for(int i = j+1;i<vertexCount;i++){
            
                    //gehe alle Verbindungen durch
                    if(distanceMatrix[getLinearizedArrayIndex(j,i,vertexCount)] != INT_MAX){
                        //Es gibt eine Verbindung
                        addedVertices[i] = 1;
                   
                    }
               
                }
            }
        }
        
        //Überprüfe ob schon alle Knoten aufgenommen wurden
        int allAdded = 1;
        for(int i = 0;i<vertexCount;i++){
            if(addedVertices[i] == 0){
                allAdded = 0;
                continue;
            }
        }
        //Alle Knoten wurden hinzugefügt, somit muss der Graph zusammenhängend sein
        if(allAdded)
            return 1;
    }
    for(int i = 0;i<vertexCount;i++)
        if(addedVertices[i] == 0)
            return 0;
    return 1;
}