#pragma once
#ifndef USERINPUTH_INCLUDED
#define USERINPUTH_INCLUDED

/* Diese Headerdatei hat die nötigen Funktionen um Userinput zu überprüfen,
 * bei Unstimmigkeiten den Nutzer darum zu bitten, erneut eine Eingabe zu tätigen
 * und bei korrekter Eingabe den Wert zurückzugeben.
 */
//Definiere true und false, da C ohne includes keine Boolschen Werte hat.
#include "color.h"
#define false 0
#define true !false
//NUL ist das Null-Zeichen, auch 0-Terminator genannt. Wird zur Notation des Endes eines Strings verwendet.
#define NUL '\0'
#include "umlaute.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/**
 * @param {char} c Überprüfe ob der Charakter ein WhiteSpace ist.
 */
int isCharAWhiteSpaceChar(char c){
	if(
		//Alle WhiteSpace Charaktere in C.
			c == ' ' ||
			c == '\n'||
			c == '\t'||
			c == '\v'||
			c == '\f'||
			c == '\r'
	)
	{
		return 1;
	}else{
		return 0;
	}
}


//Es werden von einem String am anfang und ende alle WhiteSpace-Chars entfernt
void stripWhiteSpaces(char string[]){
	//Länge des Strings bekommen. Man sucht das erste Vorkommen des \0-Charakters
	int len = 0;
	while(string[len] != NUL){
		len++;
	}
	//Bekomme das erste Vorkommen eines Non-Whitespace-Chars
	int lower = 0;

	for(;lower<len;lower++){
		if(!isCharAWhiteSpaceChar(string[lower])){
			break;
		}
	}
	
	//Bekomme das letzte Vorkommen eines Non-Whitespace-Chars
	int upper = len;
	for(;upper > 0;upper--){
		if(isCharAWhiteSpaceChar(string[upper])){
			break;
		}
	}
	int i=lower;
	//Leere die WhiteSpace-Charaktere vorne
	for(;i<(upper);i++){
		string[i-lower] = string[i];
	}
	i = upper;
	for(;i<len;i++){
		string[i] = NUL;
	}
	//printf("[%i,%i]",lower,upper);

}





//Funktion um einen String vom Nutzer anzufordern und zurückzugeben. 
//Maxlen definiert dabei die maximale Eingabelänge. 
//Wenn maxlen == 0 ist, so wird jede Länge (< des Buffers) akzeptiert.
/**
 * @returns {char*} returnPointer Zeigt auf das erste Element des übergebenen Char-Buffers
 * @param {unsigned int} maxlen Die Maximallänge des Eingabestrings. Der größtmögliche Wert ist die Größe des übergebenen Buffers -1 (wegen des \0-Terminators)
 * @param {int} allowEmptyResponse Darf die Eingabe leer sein?
 */
char* userInput_getString(unsigned int maxlen,int allowEmptyResponse){
//Fordere Nutzer auf eine Zeichenkette anzugeben.

	//Definiere einen Eingabebuffer. Die Eingabe darf nicht größer als dieser Wert sein. Das +1 wird benötigt, da man Platz für \0 benötigt. Dieser Charakter denotiert das Ende der Strings.
	char buffer[maxlen+1];
	//Führe diese Schleife aus bis eine korrekte Eingabe erfolgt. Bei Strings ist die einzige mögliche "falsche" Eingabe eine leere Eingabe.
	while(true){
		//fgets() → https://www.geeksforgeeks.org/fgets-gets-c-language/ → Speicher einen CharArray. Dabei wird die Ausgabe nie länger als der angegebene Wert.
		//Dadurch können Bufferoverflow-Angriffe verhindert werden. 

		//Lösche jegliche Eingabe im input-Buffer welche von bspw. vorherigen Abfragen übergeblieben sind.
		fflush(stdin);
		//Wenn die Funktion NULL ausgibt ist ein Fehler aufgetreten. In dem Fall soll der Nutzer erneut nach der Eingabe gefragt werden.
		if(fgets(buffer,maxlen,stdin) == NULL){
			consoleChangeColor(Error);
			printf("[FEHLER] Konnte die Eingabe nicht lesen. Bitte versuchen Sie es erneut.\n");
			consoleChangeColor(Default);
			continue;
		}
		//Hier wird die Eingabe getrimmt, dass heißt das Whitespaces vor und nach Charakteren, welche keine Whitespaces sind, entfernt werden. (whitespaces sind Leerzeichen, \n, \r usw.)
		stripWhiteSpaces(buffer);
		//Wenn die Eingabe leer ist (also der CharArr an Position 0 den \0-Charakter hat) soll der Nutzer eine neue Eingabe tätigen.
		//fgets() hat die Spezialität, dass bei leerer Eingabe \n zurückgegeben wird. Jedoch wird das \n durch das vorherige Trimmen entfernt.
		if(buffer[0] == '\0' && !allowEmptyResponse){
			consoleChangeColor(Error);
			printf("[FEHLER] Die Eingabe darf nicht leer sein. Bitte versuchen Sie es erneut.\n");
			consoleChangeColor(Default);
			continue;
		}
		//Bestimme die Länge der Eingabe, exklusive NUL
		int len = 0;
		int j = 0;
		while(buffer[j++] != NUL){
			len++;
		}
		if(len == 0)
			return NULL;

		//Überschreibe den eingegebenen String in den übergebenen Buffer. 
		//Returnpointer beinhaltet den ersten Char des Rückgabestrings
		char* returnPointer  = (char*) malloc(sizeof(char)*(len+1));
		for(int i = 0;i<len+1;i++){
			returnPointer[i] = buffer[i];
		};
		
		//Auch hier nochmal den Input-Buffer leeren.
		fflush(stdin);

		return returnPointer;

	}
}
//Funktion um einen Char vom Nutzer anzufordern und zurückzugeben. Da es nur ein Charakter und somit keine Referenzen angegeben werden müssen, kann der Charakter zurückgegeben werden.
char userInput_getChar(){
	//Fordere Nutzer auf, einen Charakter einzugeben
	consoleChangeColor(Gray);
	printf("Bitte geben sie einen Charakter ein. (Der erste Charakter der Eingabe wird uebernommen)\n");
	consoleChangeColor(Default);
	//Dennoch wird ein Buffer von mindestens 2 Elementen benötigt, da bei einem leeren Eingabe fgets() "\n\0" in den Buffer schreibt.
	char buffer[3];

	while(true){
		//Lösche jegliche Eingabe im input-Buffer welche von bspw. vorherigen Abfragen übergeblieben sind.
		fflush(stdin);
		if(fgets(buffer,3,stdin) == NULL){
			consoleChangeColor(Error);
			printf("[FEHLER] Konnte die Eingabe nicht lesen. Bitte versuchen Sie es erneut.\n");
			consoleChangeColor(Default);
			continue;
		}
		if(buffer[0] == NUL || isCharAWhiteSpaceChar(buffer[0])){
			consoleChangeColor(Error);
			printf("[FEHLER] Die Eingabe darf nicht leer sein. Bitte versuchen Sie es erneut.\n");
			consoleChangeColor(Default);
			continue;
		}
		fflush(stdin);
		return buffer[0];

	}
}

//Fordert vom Nutzer eine ganze Zahl an.
/**
 * @param {int} positiveOnly Dient als boolscher Wert. Sagt aus ob nur positive Eingabe erlaubt sind
 * @param {int} allowZero Dient als boolscher Wert. Sagt aus ob 0 als Eingabe erlaubt ist
 */
int userInput_getInt(int positiveOnly,int allowZero, int allowEmptyInput){
	consoleChangeColor(Gray);
	printf("Bitte geben sie eine ganze");
	if(positiveOnly){
		printf(", positive");
	}
	printf(" Zahl ein.\n");
	consoleChangeColor(Default);
	//Zuerst wird die Zahl als String eingelesen, und daraufhin (wenn möglich) in einen Integer konvertiert.
	char buffer[51]; //Der Input sollte nicht länger als 50 Chars sein. Alles danach wird ignoriert.
	while(true){
		//Lösche jegliche Eingabe im input-Buffer welche von bspw. vorherigen Abfragen übergeblieben sind.
		fflush(stdin);
		if(fgets(buffer,50,stdin) == NULL){
			consoleChangeColor(Error);
			printf("[FEHLER] Konnte die Eingabe nicht lesen. Bitte versuchen Sie es erneut.\n");
			consoleChangeColor(Default);
			continue;
		}


		stripWhiteSpaces(buffer);
		if(buffer[0] == NUL || isCharAWhiteSpaceChar(buffer[0])){
			if(allowEmptyInput)
				return INT_MAX;
			consoleChangeColor(Error);
			printf("[FEHLER] Die Eingabe darf nicht leer sein. Bitte versuchen Sie es erneut.\n");
			consoleChangeColor(Default);
			continue;
		}

		//Die Eingabe ist in Ordnung (d.h. nicht leer und keine Fehler). Jetzt wird überprüft, ob es sich um eine Ganze Zahl handelt.
		//String to Decimal. 
		char* endpointer;
		int value = (int)strtol(buffer,&endpointer,10);
		//Wenn alles richtig gegangen ist muss der endpointer auf einen 0-Terminator, oder auf ein WhiteSpace zeigen.
		if((*endpointer != NUL)&&(!isCharAWhiteSpaceChar(*endpointer))){
			consoleChangeColor(Error);
			printf("[FEHLER] Bei der Eingabe handelt es sich um keine Ganze Zahl.\n");
			consoleChangeColor(Default);
			continue;
		}
		if(positiveOnly){
			if(value < 0){
				consoleChangeColor(Error);
				printf("[FEHLER] Der Eingabewert muss positiv sein.\n");
				consoleChangeColor(Default);
				continue;
			}
		}
		if(!allowZero){
			if(value == 0){
				consoleChangeColor(Error);
				printf("[FEHLER] Der Eingabewert darf nicht 0 sein.\n");
				consoleChangeColor(Default);
				continue;
			}
		}
		fflush(stdin);
		return value;

	}	
}

//Fordert vom Nutzer einen Bool'schen Wert. Es wird eine Eingabe von y bzw. n erwartet.
int userInput_getBoolean(){
	consoleChangeColor(Gray);
	printf("Bitte geben sie \"Y\"(yes) oder \"N\"(no) an\n");
	consoleChangeColor(Default);
	char buffer[3];

	while(true){
		//Lösche jegliche Eingabe im input-Buffer welche von bspw. vorherigen Abfragen übergeblieben sind.
		fflush(stdin);
		if(fgets(buffer,3,stdin) == NULL){
			consoleChangeColor(Error);
			printf("[FEHLER] Konnte die Eingabe nicht lesen. Bitte versuchen Sie es erneut.\n");
			consoleChangeColor(Default);
			continue;
		}
		if(buffer[0] == NUL || isCharAWhiteSpaceChar(buffer[0])){
			consoleChangeColor(Error);
			printf("[FEHLER] Die Eingabe darf nicht leer sein. Bitte versuchen Sie es erneut.\n");
			consoleChangeColor(Default);
			continue;
		}
		if(buffer[0] != 'Y' && buffer[0] != 'y' && buffer[0] != 'N' && buffer[0] != 'n'){
			consoleChangeColor(Error);
			printf("[FEHLER] Nur Y und N d%crfen eingegeben werden.\n",ue);
			consoleChangeColor(Default);
			continue;
		}
		//Die Eingabe ist korrekt. Wenn Y oder y → return 1 ansonsten 0
		fflush(stdin);
		if(buffer[0] == 'Y' || buffer[0] == 'y'){
			return 1; 
		}else{
			return 0;
		}
	}
}

#endif