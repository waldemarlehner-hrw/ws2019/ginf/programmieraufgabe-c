#pragma once
#ifndef OUTPUTH_INCLUDED
#define OUTPUTH_INCLUDED

// Diese Headerdatei hat die nötigen Funktionen für die Ausgabe; bspw. die Ausgabe der Matrix.
#include <stdio.h>
#include <limits.h> //Benötigt für INT_MAX. Kann unter verschiedenen Systemen verschiedene Werte
#include <math.h>
#include "umlaute.h"
#include "matrix.h"
#include "unionfind.h"
#include "kruskalPrepare.h"
//Gibt die Gruppennummer und kurze Beschreibung wieder.
void printInitOutput(){
	printf(
		"//////////////////////////////////////////\n"
		"Kruskal-Algorithmus\n"
		"Gruppe 7 - Wintersemester 2019\n"
		"//////////////////////////////////////////\n"
		"\n\n\n"
	);
}

//Gibt die Länge einer Zahl in Charakteren zurück. Benötigt für die Darstellung in der Tabelle
int getIntLen(int val){
	if(val == 0 || val == INT_MAX){ //0 und Unendlich werden jew. durch 1 Char repräsentiert.
		return 1;
	}
	int isPositive = true;
	if(val < 0){
		isPositive = false;
		val *= -1;
	}
	int len = (int)log10(val)+1;
	if(!isPositive){
		len++;
	}
	return len;
}
//Gibt die Länge eines Strings (inkl. \0 an)
int getStringLen(char* string){
	int len = 0;
	while(string[len++] != NUL);
	return len;
}
//Geht durch den Namen-Array und gibt die Länge des Längsten Strings aus
int getMaxNameLen(char** strings,int vertexCount){

	int maxlen = 0;
	for(int i=0;i<vertexCount;i++){

		int len = getStringLen(strings[i]);
		if(len > maxlen){
			maxlen = len;
		}
	}
	return maxlen;
}


//Gaukelt eine Säuberung der Konsole vor. Es gibt zwar system("clear") bzw system("cls"), jedoch hängt dies von der Umgebung ab.
void clear(){

	for(int i = 0; i < 5;i++){
		printf("\n\n\n\n\n\n\n\n\n\n");
	}
}

//Schreibe X Chars in die Konsole
void putXSpaces(int spaceCount){
	while(spaceCount-->0){
		putchar(' ');
	}
}
//Gebe die Legende für die Abstandsmatrix aus
void printDistanceMatrixLegend(){
		//Legende Darstellen
	consoleChangeColor(Info);
	printf("[Legende]\n");
	consoleChangeColor(Warn);
	printf("*");
	consoleChangeColor(Info);
	printf("  zeigt an, dass es sich um das Element selbst handelt.\n");// = 0
	consoleChangeColor(Gray);
	printf("-");
	consoleChangeColor(Info);
	printf("  zeigt an, dass keine Verbindung zwischen beiden Knoten besteht.\n");	// = INT_MAXVALUE			
	
		

	consoleChangeColor(Default);
}

//Gebe die Abstandsmatrix schön Strukturiert aus
void printDistanceMatrix(int verticesCount,char** namesArray, int distanceMatrixLinearized[]){
	//Speichert für jede Spalte die Maximallänge der Werte (bestimmt mit log_10), um eine Verzerrung der Darstellung der Matrix zu verhindern.
	int maxLenPerColumn[verticesCount];
	int LenOfIndexColumn = getIntLen(verticesCount+1); //siehe: https://www.desmos.com/calculator/htt77soqjx; Gibt die Maximale Länge der Spalte, in welcher die Indizes stehen, an
	int LenOfNames = getMaxNameLen(namesArray,verticesCount);
	if(LenOfNames > 20){
		LenOfNames = 20; //Damit man nicht unnötig lange Namen hat werden diese nach 20 chars abgeschnitten
	}
	

	//Bekomme die maximalen Längen der einzelnen Spalten
	// j = Spalte
	for(int spalte = 0;spalte<verticesCount;spalte++){
		//Bekomme den größten Wert aus jeder Spalte
		int maxLen = 0; //Gibt die Maximallänge in dieser Spalte aus
	
		for(int zeile = 0;zeile < verticesCount;zeile++){
			int len = getIntLen(distanceMatrixLinearized[getLinearizedArrayIndex(zeile,spalte,verticesCount)]+1);
			maxLen = (len > maxLen) ? len : maxLen; 
		}
		maxLenPerColumn[spalte] = maxLen;

	}
	consoleChangeColor(Info);
	//Zeilenkopf aufbauen
	printf("----------------------------------------------------------------------\n");
	putXSpaces(LenOfNames);
	printf("   ");
	putXSpaces(LenOfIndexColumn);
	printf(" | ");
	
	//Platziere den Index in die Indizes-Spalte
	for(int i=0;i<verticesCount;i++){
		printf("[");
		//i = Index des Knotens
		int indexLen = getIntLen(i+1);		//Das ist die (Zeichen)länge des Indexes
		int totalLen = maxLenPerColumn[i];						//Das ist die Länge, welche der Eintrag haben muss, um eine schöne Formatierung zu erzeugen
		putXSpaces(totalLen-indexLen);							//Fülle vor der Ausgabe des Indexes mit Leerzeichen voll
		printf("%i]  ",i+1);
	}
	printf("\n----------------------------------------------------------------------\n");
	consoleChangeColor(Default);
	//Kopf fertig.
	//Drücke nun alle Zeilen aus.
	
	for(int i=0;i<verticesCount;i++){
		#pragma region KnotenName
		int nameLen = getStringLen(namesArray[i]); //Länge des Namens. Um den Text linksbündig darzustellen und Spaces "vorzufüllen"


		//Schreibe den Namen und // zeichen
		consoleChangeColor(Info);
		putXSpaces(LenOfNames-nameLen);
		printf("%.20s  [",namesArray[i]);
		//Bekomme die Maximallänge der Indizes und die Länge des Indexes. Fülle die Differenz mit Leerzeichen auf.
		int indexlen = getIntLen(i+1);
		putXSpaces(LenOfIndexColumn-indexlen);
		printf("%i]| ",i+1);
		consoleChangeColor(Default);
		#pragma endregion
		//Der linke Teil ist fertig. Jetzt kommt die Darstellung der einzelnen Werte
		 //Iteriere nach Rechts in der Matrix
		for(int j = 0;j<verticesCount;j++){
			int value = distanceMatrixLinearized[getLinearizedArrayIndex(i,j,verticesCount)];
			int maxLen = maxLenPerColumn[j];
			int valLen; //Die Laenge des Wertes in Chars

			valLen = getIntLen(value);
		
			

			if(value == INT_MAX){
				consoleChangeColor(Gray);
				printf("[");
			}
			else if(value == 0 && i == j){
				consoleChangeColor(Warn);
				printf("[");
			}else{
				printf("[");
			}

			



			putXSpaces(maxLen-valLen);
			if(value == INT_MAX){
				printf("-]  ");
			}
			else if(value == 0 && i == j){
				printf("*]  ");
			}else{
				printf("%i]  ",value);
			}
			consoleChangeColor(Default);
		}
		
		printf("\n");


	}
	consoleChangeColor(Info);
	printf("----------------------------------------------------------------------\n");
	consoleChangeColor(Default);
	

}



//Gebe die Kanten in einer Tabelle aus. Die Kanten sollten vor dem Aufruf dieser Funktion sortiert werden
void printEdgeTable(GRAPH_EDGE* edges, int edgeCount,char** stringnames){
	
	
	//Header
	// Name1 [ID1] <-> Name2 [ID2] 	| WEIGHT
	// max 32c / name 				| max 10c
	
	//Bekomme die maximas für die richtige Formatierung
	int maxVertexID1 = -1;
	int maxVertexID2 = -1;
	int name1maxLen = 0;
	int name2maxLen = 0;
	int totalnameLen = 0;
	for(int i = 0;i<edgeCount;i++){
		int name1Len = getStringLen(stringnames[edges[i].VERTEX1]);
		int name2Len = getStringLen(stringnames[edges[i].VERTEX2]);
		if(name1Len > name1maxLen)
			name1maxLen = name1Len;
		if(name2Len > name2maxLen)
			name2maxLen = name2Len;
		if(maxVertexID1 < edges[i].VERTEX1)
			maxVertexID1 = edges[i].VERTEX1;
		if(maxVertexID2 < edges[i].VERTEX2)
			maxVertexID2 = edges[i].VERTEX2;

		
	}
	if(name1maxLen > 32)
		name1maxLen = 32;
	if(name2maxLen > 32)
		name2maxLen = 32;
	totalnameLen = name1maxLen+name2maxLen+4/*[][]*/+5/* <-> */+getIntLen(maxVertexID1)+getIntLen(maxVertexID2);
	
	//Beginne mit der Ausgabe:

	//Header

	consoleChangeColor(Info);
	printf("----------------------------------------------------------------------\n");
	putXSpaces(getIntLen(edgeCount));
	printf(" | ");
	putXSpaces(totalnameLen);
	printf("| Kantengewicht\n");
	printf("----------------------------------------------------------------------\n");
	maxVertexID1 = getIntLen(maxVertexID1);
	maxVertexID2 = getIntLen(maxVertexID2);
	for(int i=0;i<edgeCount;i++){
		consoleChangeColor(Info);
		putXSpaces(getIntLen(edgeCount)-getIntLen(i+1));
		printf("%i |[",i+1);
		putXSpaces(maxVertexID1 - getIntLen(edges[i].VERTEX1+1));												//1. index
		printf("%i] %.*s",edges[i].VERTEX1+1,name1maxLen,stringnames[edges[i].VERTEX1]);
		{
			int spacesToPut = name1maxLen - getStringLen(stringnames[edges[i].VERTEX1]);
			if(spacesToPut > 0)
				putXSpaces(spacesToPut);

		}		//1. name
		

		printf(" <-> [");
		putXSpaces(maxVertexID2 - getIntLen(edges[i].VERTEX2+1));
		printf("%i] %.*s",edges[i].VERTEX2+1,name2maxLen,stringnames[edges[i].VERTEX2]);
		{
			int spacesToPut = name2maxLen - getStringLen(stringnames[edges[i].VERTEX2]);
			if(spacesToPut > 0)
				putXSpaces(spacesToPut);
		}		//2. name
		printf(" | ");
		consoleChangeColor(Default);
		printf("%i\n",edges[i].WEIGHT);
		consoleChangeColor(Info);
		


	}
	printf("\n\n\n");
	

}

//Es wird die Tabelle mit alles Kruskal-Iterationen ausgegeben. 
void printKruskal(char** vertexNames,int vertexCount,int edgeCount, UF_Reference* UnionFindMap,Kruskal_Table_Entry* kruskalTableEntries,GRAPH_EDGE* Edges){
	//Bestimme zuerst die Maximalwerte, um eine strukturierte Ausgabe zu ermöglichen. Dafür müssen die Namen leider abgeschnitten werden
	int maxV1len = 0;
	int maxV2len = 0;
	int maxV1namelen = 3;
	int maxV2namelen = 2;
	int maxWeightlen = 7;
	int maxVertexlen = getIntLen(vertexCount);
	int maxV1Replen = 7;
	int maxV2Replen = 6;
	int maxIterationLen = 5;
	//Gehe durch alle Kanten, schaue dabei die Knoten und die Namen an, sowie das Gewicht, und bestimme die maximale Länge
	for(int i = 0;i<edgeCount;i++){
		int v1 = Edges[i].VERTEX1;
		int v2 = Edges[i].VERTEX2;
		int w = Edges[i].WEIGHT;
		if(getIntLen(v1) > maxV1len)
			maxV1len = getIntLen(v1+1);
		if(getIntLen(v2) > maxV2len)
			maxV2len = getIntLen(v2+1);
		if(getIntLen(w) > maxWeightlen)
		maxWeightlen = getIntLen(w);
		int v1namelen = getStringLen(vertexNames[v1]);
		int v2namelen = getStringLen(vertexNames[v2]);
		if(maxV1namelen < v1namelen)
			maxV1namelen = v1namelen;
		if(maxV2namelen < v2namelen)
			maxV2namelen = v2namelen;
	}
	//Setze obere und untere Grenzen für Längen
	maxIterationLen = (maxVertexlen > maxIterationLen) ? maxVertexlen : maxIterationLen;
	maxV1namelen = (maxV1namelen > 31) ? 30 : maxV1namelen;
	maxV2namelen = (maxV2namelen > 31) ? 30 : maxV2namelen;
	if(maxV1Replen < maxVertexlen)
		maxV1Replen = maxVertexlen;
	if(maxV2Replen < maxVertexlen)
		maxV2Replen = maxVertexlen;



	//Gebe nun den Header aus
#pragma region Header
	consoleChangeColor(Info);
	printf("# > Aufgenommen in MST\n");
	consoleChangeColor(Error);
	printf("# > Nicht Aufgenommen in MST\n\n\n\n");
	consoleChangeColor(Default);

	printf("-----------------------------------------------------------------------------------\n");
	printf("Iter.");
	putXSpaces(maxIterationLen-5);
	printf(" | ");
	putXSpaces(maxV1len+2); //2 für [ ]
	printf("Von");
	putXSpaces(maxV1namelen-3);
	printf(" | ");

	printf("Gewicht");
	putXSpaces(maxWeightlen - 7);
	printf(" | ");
	putXSpaces(maxV2len+2); //2 für [ ]
	printf("Zu");
	putXSpaces(maxV2namelen-2);
	printf(" || Rep Von");
	putXSpaces(maxV1Replen-7);
	printf(" | Rep Zu");
	putXSpaces(maxV2Replen-6);
	printf("\n");
	printf("-----------------------------------------------------------------------------------\n");
#pragma endregion
#pragma region Row
	for(int i = 0;i<edgeCount;i++){
		Kruskal_Table_Entry entry = kruskalTableEntries[i];
		if(entry.hasbeenAdded){
			consoleChangeColor(Info);
		}else{
			consoleChangeColor(Error);
		}
		//Iter.
		putXSpaces(maxIterationLen-getIntLen(i+1));
		printf("%i | [",i+1);
		// V1
		putXSpaces(maxV1len-getIntLen(entry.v1+1));
		// V1 & V1Name
		printf("%i] %.*s",entry.v1+1,maxV1namelen,vertexNames[entry.v1]);
		putXSpaces(maxV1namelen-getStringLen(vertexNames[entry.v1]));
		printf(" | ");
		// Weight
		putXSpaces(maxWeightlen-getIntLen(entry.weight));
		printf("%i | [",entry.weight);
		// V2
		putXSpaces(maxV2len-getIntLen(entry.v2+1));
		// V2 & V2Name
		printf("%i] %.*s",entry.v2+1,maxV2namelen,vertexNames[entry.v2]);
		putXSpaces(maxV2namelen-getStringLen(vertexNames[entry.v2]));
		printf(" || ");
		putXSpaces(maxV1Replen - getIntLen(entry.rep1+1));
		printf("%i | %i",entry.rep1+1,entry.rep2+1);
		//Länge für Rep2 nicht benötigt, da End of Line.
		printf("\n");
		consoleChangeColor(Default);


		
	}
#pragma endregion
	

}
#endif
