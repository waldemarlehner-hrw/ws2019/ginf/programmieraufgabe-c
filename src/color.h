
#ifndef COLORH_INCLUDED
#define COLORH_INCLUDED

#pragma once
enum ConsoleColors {
	Default,
	Info,
	Warn,
	Error,
	Gray
};

//Header zum ändern der Farbausgabe.
//Ist Windows
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
	#define IS_WINDOWS_ENVIRONMENT
#endif

#ifdef IS_WINDOWS_ENVIRONMENT
	#include <windows.h>
	//Quelle hiefür: https://faq.cprogramming.com/cgi-bin/smartfaq.cgi?answer=1048382857&id=1043284392
	HANDLE windowsColorHandle;
	WORD defaultConsoleAttribures;
	CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
#endif

void consoleChangeColor(int color){
	#ifdef IS_WINDOWS_ENVIRONMENT
		int colorId = 0;
		//Farbcodes von http://smallvoid.com/article/winnt-command-prompt-color.html
		switch(color){
			case Default: 
			colorId = defaultConsoleAttribures;
			break;
			
			case Info:
			colorId = 0x3;
			break;
			
			case Warn:
			colorId = 0x6;
			break;
			
			case Error:
			colorId = 0xC;
			break;

			case Gray:
			colorId = 0x8;
			break;
		}
		SetConsoleTextAttribute(windowsColorHandle,colorId);
	#else
	//Linux  http://web.theurbanpenguin.com/adding-color-to-your-output-from-c/
		switch(color){
			case Default: 
			printf("\033[0m");
			break;

			case Info:
			printf("\033[0;34m");
			break;
			
			case Warn:
			printf("\033[0;33m");
			break;
			
			case Error:
			printf("\033[0;31m");
			break;
			
			case Gray:
			printf("\033[30;1m");
			break;
		}
		
	#endif
}
#endif